﻿using Service1.App.ViewModels;
using Service1.Domain.Entities;
using Service1.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service1.App.Services
{
    public class ContractApp
    {
        public ResponseViewModel<Tax> GetTaxes()
        {
            ResponseViewModel<Tax> response = new ResponseViewModel<Tax>();
            try
            {
                response.Data = new Tax { Value = decimal.Parse(Constants.TaxesResult) };
                response.Error = false;

                return response;
            }
            catch (Exception ex)
            {
                response.Error = true;
                response.Message = ex.Message;

                return response;
            }
        }
    }
}
