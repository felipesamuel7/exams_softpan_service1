﻿namespace Service1.App.ViewModels
{
    public class ResponseViewModel<T>
    {
        public T Data { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
    }
}
