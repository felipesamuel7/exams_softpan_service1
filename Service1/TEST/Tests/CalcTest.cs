﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Service1.App.Services;
using Service1.Shared;

namespace Service1.UnitTest.Tests
{
    [TestClass]
    public class CalcTest
    {
        private ContractApp contractApp;

        public CalcTest()
        {
            contractApp = Substitute.For<ContractApp>();
        }

        [TestMethod]
        public void GetTaxes()
        {
            decimal tax = decimal.Parse(Constants.TaxesResult);
            var response = contractApp.GetTaxes();

            Assert.AreEqual(response.Data.Value, tax);
        }
    }
}
