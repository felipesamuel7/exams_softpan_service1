﻿using System;
using Microsoft.AspNetCore.Mvc;
using Service1.App.Services;

namespace Service1.API.Controllers
{
    public class CalcController : ControllerBase
    {
        /// <summary>
        /// Retorna valor da Taxa de Juros
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        [Route("/taxaJuros")]
        [HttpGet]
        public IActionResult Get([FromServices] ContractApp app)
        {
            try
            {
                return Ok(app.GetTaxes());
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
